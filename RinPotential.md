Utilidades do R - Conceitos Básicos e Aplicações
========================================================
author: André Fonseca - Doutorado de Bioinformática (PPG-BIOINFO)
date: 10 de Março de 2017
autosize: true
font-family: 'Helvetica'
class: illustration
transition: concave

Objetivo
========================================================

## Sair com conhecimentos plenos em R? **Não**

Vamos apenas arranhar um pouco da superfície do R! A proposta dessa aula é apresentar o R:

- Contexto histórico e na bioinformática
- Explicar superficialmente as estruturas de dados
- Em que contexto é vantajoso usar o R
- Quais são as boas práticas e o que deve evitado :)
- O que se pode desenvolver utilizando essa linguagem
- Apresentar algumas ferramentas ou pacotes utéis 
- Praticar um pouquinho de R!

Sumário
========================================================

- Histórico do R e R-Studio
- Paradigmas de Programação em R
- Declaração de variáveis
- Estrutura de Dados no R
    - Data.frame
- Funções vs Loops
- Desenvolvimento no R
- Dinâmica em Sala
    - Trabalhar com data.frame e bibliotecas gráficas

### Para ter acesso ao conteúdo da aula e outros matérias, <https://goo.gl/cUNsZQ>.

Histórico do R e R-Studio
========================================================

- Nascido a partir da linguagem S (1997)
- O R é uma linguagem e um ambiente para análise estatística e gráfica
  - É uma linguagem interpretada
  - É expansível - pacotes e funções
- Repositório oficial CRAN e [Bioconductor][id1] (Bioinformática)
- Existem várias GUI (graphical user interface) para R
  - JGR, RKWard, SciViews-R, Rcmdr e o **RStudio**
      - https://www.rstudio.com/

[id1]: http://bioconductor.org/packages/release/BiocViews.html#___Software

Paradigmas de Programação em R
========================================================

- Programação Estruturada / Funcional
  - "R, at its heart, is a functional programming (FP) language"
  - As estruturas e funções são o fundamento da linguagem
- Programação Orientada a Objetos
  - Historicamente diversa - funções S3, S4 e RC
      - [R Avançado](http://adv-r.had.co.nz/OO-essentials.html)
- Programação Paralela
  - **SNOW**, foreach, doParallel

Sumário
========================================================
- Histórico do R e R-Studio
- Paradigmas de Programação em R
- **Declaração de variáveis**
- Estrutura de Dados no R
    - Data.frame
- Funções vs Loops
- Desenvolvimento no R
- Dinâmica em Sala
    - Trabalhar com data.frame e bibliotecas gráficas

Declaração de variáveis
========================================================
Alguns princípios básicos da programação em R
- É uma linguagem de tipagem fraca
- Praticamente tudo é armazenado em vetores
- Operador de atribuição
  - Existem duas formas **=** ou **<-**
- Comentarios em R são iniciados com "#"

Estrutura de Dados no R
========================================================
### Vetores e Matrizes
Estruturas básicas do R. Compreendem os tipos mais basais, normalmente, contendo elementos de apenas um tipo. A única diferença entre vetores e matrizes está nas dimensões.
- Vetores são declarados através da instrução, *c() ou as.vector()*
- Exemplo de uma matriz declarada

```
     [,1] [,2] [,3]
[1,]    2    3    5
[2,]    4    1    7
```
***Um importante fundamento do R está relacionado a como acessar suas [estruturas][url1]. Por exemplo, matrix[1,2]***

[url1]: https://s-media-cache-ak0.pinimg.com/originals/aa/de/f2/aadef23ec6f0f7d20c7807fe609de645.jpg

Estrutura de Dados no R
========================================================

### Listas e Data.frame

São estruturas secundárias. Em particular, **listas** são abstrações mais complexas de vetores. Listas ou Data.frames,
podem conter elementos de diversos tipos, incluindo até outras estruturas.


```r
listname = list(1:3, "a", c(TRUE, FALSE, TRUE), c(2.3, 5.9))
str(listname)
```

```
List of 4
 $ : int [1:3] 1 2 3
 $ : chr "a"
 $ : logi [1:3] TRUE FALSE TRUE
 $ : num [1:2] 2.3 5.9
```

Data.frame - "Planilhas" do R
========================================================
Data.frame são estruturas baseadas em listas associadas. Apresentando duas dimensões, em que cada atributo corresponde a uma coluna e seus valores são células.


```r
flintstones = data.frame(names=c('Wilma', 'Fred', 'Bam-bam'), age=c('31','34', '8'), jobs=c('wife', 'husband', 'child'), sex=c('female', 'male', 'male'))

flintstones
```

```
    names age    jobs    sex
1   Wilma  31    wife female
2    Fred  34 husband   male
3 Bam-bam   8   child   male
```

Data.frame - "Planilhas" do R
========================================================
Operador "**$**" de acesso a coluna

```r
flintstones$names
```

```
[1] Wilma   Fred    Bam-bam
Levels: Bam-bam Fred Wilma
```

Consultar o nome das colunas 


```r
colnames(flintstones)
```

```
[1] "names" "age"   "jobs"  "sex"  
```


Data.frame - "Planilhas" do R
========================================================

Selecionando o contéudo de um Data.frame

```r
flintstones[flintstones$names == 'Fred',]
```

```
  names age    jobs  sex
2  Fred  34 husband male
```

```r
flintstones[flintstones$names == 'Fred', c('names', 'age')]
```

```
  names age
2  Fred  34
```

Sumário
========================================================

- Histórico do R e R-Studio
- Paradigmas de Programação em R
- Declaração de variáveis
- Estrutura de Dados no R
    - Data.frame
- **Funções vs Loops**
- Desenvolvimento no R
- Dinâmica em Sala
    - Trabalhar com data.frame e bibliotecas gráficas

Funções vs Loops no R
========================================================
As funções em R são os principais métodos para acesso e manipulação de dados. Na verdade, alguns procedimentos apenas se tornam eficazes se realizados com funções. Por exemplo, calcular a média de +5000 elementos.

<img src="RinPotential-figure/unnamed-chunk-4-1.png" title="plot of chunk unnamed-chunk-4" alt="plot of chunk unnamed-chunk-4" style="display: block; margin: auto;" />

Funções vs Loops no R
========================================================
Observando a seguinte estrutura, como seria possível criar uma sexta coluna, nomeada como **Sepal.Total**, com o resultado da operação *Sepal.Length x Sepal.Width* ? Em seguida, para cada espécie na coluna **Species**, calcular a média do Sepal.Total.


| Sepal.Length| Sepal.Width| Petal.Length| Petal.Width|Species |
|------------:|-----------:|------------:|-----------:|:-------|
|          5.1|         3.5|          1.4|         0.2|setosa  |
|          4.9|         3.0|          1.4|         0.2|setosa  |
|          4.7|         3.2|          1.3|         0.2|setosa  |
|          4.6|         3.1|          1.5|         0.2|setosa  |
|          5.0|         3.6|          1.4|         0.2|setosa  |

***Existe um loop no seu código? Se sim, ele pode ser otimizado.***

Funções vs Loops no R
========================================================
O problema anterior pode ser solucionado com apenas duas linha de código em R. Segue abaixo.

```r
iris$Sepal.Total = iris$Sepal.Length * iris$Sepal.Width

by(iris, list(iris$Species), function(x) mean(x$Sepal.Total))
```

Imagine situações em que é necessário percorrer arquivos com vários **gigabytes**, ao passo que são realizadas operações matemáticas ou buscas por padrões específicos. Além disso, podem existir análises em que é preciso agrupar os dados em categorias e executar testes estatísticos para filtrar e/ou gerar gráficos.

========================================================

### Contabilizar a frequencia de determinado valor ou categoria

- table() - Retorna uma tabela de frequencia

### Transpor uma matriz ou data.frame

- t() - Inverte colunas por linhas e vice-versa

### Filtrar um data.frame a partir de um valor uma ou mais colunas

- subset() - Seleciona linhas que atenderem as condições exigidas

### Escrever linhas dinamicamente em um data.frame

- rbind() - Adiciona linhas ou registros em um data.frame pré-existente

========================================================

### Obter estatísticas descritivas de um data.frame

- summary() - Retorna média, mediana e quartis de cada coluna do data.frame

### Unir data.frames que apresentam colunas com valores iguais

- merge() - Faz a união de data.frame que compartilhem informações comuns

### Ordenar o data.frame por coluna

- order() - Retorna um vetor de posições baseado na ordenação de uma coluna

### Ver as *n* primeiras ou últimas linhas de um data.frame

- head() ou tai() -  Retorna as 10 primeiras ou últimas linhas de um data.frame

Desenvolvimento no R
========================================================
- **R Shiny**
 
 Framework para construção de aplicativos web. Implementa nativamente Bootstrap CSS e Javascript. [HTML Widgets][id3], [Shiny App][id4], [Shiny Galery][id5]
    
- **R Markdown & Presentation**

 Basicamente é uma linguagem de marcação que converte seu texto para XHTML.
 É possível construir *relatórios interativos ou estáticos* até *apresentações*, inclusive está aula foi montanda ***inteiramente*** usando R.

 Documentação: [R Markdown][id1] ou [Markdown Quick Reference][id2]

[id1]: http://rmarkdown.rstudio.com/
[id2]: http://geog.uoregon.edu/bartlein/courses/geog607/Rmd/MDquick-refcard.pdf
[id3]: https://rpubs.com/jcheng/leaflet-layers-example
[id4]: http://shiny.rstudio.com/gallery/absolutely-positioned-panels.html
[id5]: http://shiny.rstudio.com/gallery/

Desenvolvimento no R
========================================================

- **Otimização em R**

Existe algumas bibliotecas que podem auxiliar a otimização de códigos em R. Por exemplo, **microbenckmark**, **pryr** (uso de memoria). Além disso, as funções **apply** podem ser usadas para substituição de loops. Por fim, é possível utilizar APIs para execução de rotinas em C++ dentro do R, **Rcpp**.

- **Construção de Pacotes**  

A construção de pacotes em R é feita através do paradgima de orientado a objetos. Existem pacotes que auxiliam na organização do código e documentação, **skeleton** e **roxygen2**, respectivamente. Ambos disponíveis nativamente no R-Studio.
  
Dinâmica da aula
========================================================
- A turma deve se dividir em duplas
  - Preferivelmente, alunos de TI com parceiros da Biomédica
- Em seguida, acessem o endereço: http://10.7.4.225:8787
- Tela de login no R-Studio Server
  - Username: **aluno{1..20}**
  - Password: **biomecourses**
- Internamente selecionar uma pasta por dupla

========================================================

# Prática 01

- Apresentar a ferramenta RStudio e seus ambientes
- Carregar e visualizar dados de expressão no R
- Gerar estatística descritiva por tecido
- Calcular a média da expressão por gene entre os tecidos
- Selecionar genes e gerar gráficos interativos usando **plottly**
