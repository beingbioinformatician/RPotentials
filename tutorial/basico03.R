## Estrututas de dados -- Curso: R para Aspirantes a bioinformatas
## Andre Fonseca (2016)

## Data.frame -- Exemplos -- Parte 1

# Construindo um data.frame()
flintstones <- data.frame(names=c('Wilma', 'Fred', 'Bam-bam'), 
                         age=c(31,34, 8), 
                         jobs=c('wife', 'husband', 'child'),
                         sex=c('female', 'male', 'male'))

# Visualizando data.frame()
View(flintstones)
str(flintstones)

# Removendo fatores
flintstones <- data.frame(names=c('Wilma', 'Fred', 'Pedrita'), 
                         age=c('31','34', '8'), 
                         jobs=c('wife', 'husband', 'child'),
                         sex=c('female', 'male', 'female'),
                         stringsAsFactors = FALSE)
str(flintstones)
flintstones$age > 30

# Transformando uma coluna em um factor
flintstones$sex <- as.factor(flintstones$sex)
str(flintstones)

# Testes 
typeof(flintstones)
class(flintstones)
is.data.frame(flintstones)

# Adicionando novas linhas no data.frames()
rubble = data.frame(names=c('Betty', 'Barney', 'Bam-bam'), age=c('30', '33', '7'), 
                    jobs=c('wife','husband', 'child'), sex=c('female', 'male', 'male'))

flintstones = rbind(flintstones, rubble)
str(flintstones) # actor w/ 2 levels "female","male": 1 2 2 1 2 1
