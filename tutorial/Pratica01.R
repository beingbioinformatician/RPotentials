# Prática 01

## Carregando dados e Visualizando

#setwd('/home/alunos/aluno{1..20}') # Mudar pelo número do usuário
expressiondata <- read.table(file = './IlluminaBody.final.tsv', header = T, sep = '\t') # Adicionar caminho até a pasta
View(expressiondata)

## Unindo informações

ensembl2gene <- read.table(file = './ensembl2Genename.tsv', header = T, sep = '\t')
View(ensembl2gene)

expressiondata <- merge(expressiondata, ensembl2gene, by = 'Gene.ID') # Une os data.frames através de uma coluna comum
colnames(expressiondata)
expressiondata <- expressiondata[, c(1, 18, 2:17)] # Reorganizar as colunas

# x[LINHAS, COLUNAS]

## Visualizando distribuição dos dados e estatística descritiva

summary(expressiondata[, -c(1,2)]) # Escolhe todas as colunas com exceção da primeira e segunda
head(expressiondata[, -c(1,2)]) # Imprime apenas as primeiras linhas do data.frame expressiondata

## Selecionando gene especifico e plotando gráfico

expression2plot <- subset(expressiondata, Gene.Name == 'TP53')
expression2plot <- expression2plot[, -c(1, 2)] # Remove a primeira e a segunda coluna

t(expression2plot)

expression2plot <- as.data.frame(t(expression2plot), stringsAsFactors = FALSE) # Transpõe o data.frame, invertendo colunas pelas linhas

expression2plot$tissue <- NA
expression2plot$tissue <- rownames(expression2plot) # Cria uma coluna com o nome dos tecidos

rownames(expression2plot) <-  NULL # Limpa os rownames e retorna a posição numerica
colnames(expression2plot) <- c('expression', 'tissue') # Renomeia as colunas

## Carregando bibliotecas e gerando gráfico

if(!require('plotly')) {
  install.packages('plotly')
}

if(!require('ggplot2')) {
  install.packages('ggplot2')
}

library('plotly')
library('ggplot2')

dotchart <- ggplot(expression2plot, aes(x = tissue, y = expression))
dotchart + geom_point() + theme_bw() # Gera gráfico estatico 

gene4mean <- mean(expression2plot$expression) # Calculando média por gene
dotchart + geom_point() + geom_hline(yintercept = gene4mean) + 
  theme_bw() # Gera gráfico estatico + valor médio

## Construir gráfico interativo

dotchart_interactive <- ggplot(expression2plot, aes(x = tissue, y = expression))
dotchart_interactive <- dotchart_interactive + geom_point() + 
  geom_hline(yintercept = gene4mean) + theme_bw() # Gera gráfico estatico + valor médio
ggplotly(dotchart_interactive)
